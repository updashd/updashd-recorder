<?php
chdir(__DIR__);

set_error_handler('errorHandler');

function errorHandler($severity, $message, $filename, $lineno) {
    if (error_reporting() == 0) {
        return;
    }

    if (error_reporting() & $severity) {
        throw new ErrorException($message, 0, $severity, $filename, $lineno);
    }
}

require 'vendor/autoload.php';

$config = include 'config/config.default.php';

$env = getenv('ENVIRONMENT') ?: 'local';
$envConfigFileName = 'config/config.' . $env . '.php';

if (file_exists($envConfigFileName)) {
    $envConfig = include $envConfigFileName;
    
    $config = array_replace_recursive($config, $envConfig);
}

try {
    $recorder = new \Updashd\Recorder($config);
    $recorder->run();
}
catch (\Exception $e) {
    fprintf(STDERR, '%s %s %s', $e->getMessage(), str_repeat(PHP_EOL, 2), $e->getTraceAsString());
    exit(10);
}