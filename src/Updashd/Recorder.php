<?php
namespace Updashd;

use Doctrine\ORM\Query\Expr\Join;
use Predis\Client;
use Updashd\Doctrine\Manager;
use Updashd\Model\Account;
use Updashd\Model\Incident;
use Updashd\Model\MetricType;
use Updashd\Model\NodeService;
use Updashd\Model\NodeServiceZone;
use Updashd\Model\ResultLatest;
use Updashd\Model\ResultMetric as ResultMetricModel;
use Updashd\Model\Severity;
use Updashd\Model\Zone;
use Updashd\Model\Result as ResultModel;
use Updashd\Scheduler\Model\Task;
use Updashd\Scheduler\Scheduler;
use Updashd\Scheduler\Model\Error as SchedulerError;
use Updashd\Scheduler\Model\Result as SchedulerResult;
use Updashd\Scheduler\Model\Task as SchedulerTask;
use Updashd\Worker\Result as WorkerResult;

class Recorder {
    const CORRELATION_INTERVAL = '-1 day';
    const RESULT_MAX_STRING_LEN = 100;

    private $config;
    private $client;
    private $manager;
    
    public function __construct ($config) {
        // Store the configuration
        $this->setConfig($config);
    
        // Create the Predis Client
        $client = new Client($this->getConfig('predis'));
        $this->setClient($client);

        $manager = new Manager($config['doctrine']);
        $this->setManager($manager);
    }
    
    /**
     * @param Task $task
     * @param SchedulerResult|SchedulerError $result
     * @param string $severityId
     * @param WorkerResult $workerResult
     * @return null|Incident
     */
    protected function recordIncident ($task, $result, $severityId, $workerResult = null) {
        $em = $this->getManager()->getEntityManager();

        $account = $em->getReference(Account::class, $task->getAccountId());
        $zone = $em->getReference(Zone::class, $this->getConfig('zone'));
        $severity = $em->getReference(Severity::class, $severityId);

        // If the worker experienced a configuration or runtime error
        if ($result instanceof SchedulerError) {
            /** @var SchedulerError $result */
            $date = new \DateTime('@' . $result->getDate(), new \DateTimeZone('UTC'));
//            $nodeService = $em->getReference(NodeService::class, $result->getId());
            $nodeService = $this->getNodeServiceFromTaskId($result->getId());
            $errorCode = $result->getErrorCode() ? : 0;
            $errorMessage = $result->getErrorDetails() ? : 'No Error Message Provided';
        }
        // If the worker return a non-success status
        elseif ($workerResult && $result instanceof SchedulerResult) {
            /** @var SchedulerResult $result */
            $date = new \DateTime('@' . $result->getStartTime(), new \DateTimeZone('UTC'));
//            $nodeService = $em->getReference(NodeService::class, $result->getTaskId());
            $nodeService = $this->getNodeServiceFromTaskId($result->getTaskId());
            $errorCode = $workerResult->getErrorCode() ? : 0;
            $errorMessage = $workerResult->getErrorMessage() ? : 'No Error Message Provided';
        }
        else {
            $date = new \DateTime('now', new \DateTimeZone('UTC'));
            $nodeService = null;
            $errorCode = 0;
            $errorMessage = 'Unknown result type.';
        }

        $qb = $em->createQueryBuilder();
        $qb->select('i')
            ->from(Incident::class, 'i')
            ->innerJoin('i.zone', 'z')
            ->innerJoin('i.severity', 's')
            ->innerJoin('i.account', 'a')
            ->where($qb->expr()->andX(
                $qb->expr()->eq('i.nodeService', ':nodeService'),
                $qb->expr()->eq('i.zone', ':zone'),
                $qb->expr()->eq('i.severity', ':severity'),
                $qb->expr()->eq('i.account', ':account'),
                $qb->expr()->eq('i.messageCode', ':messageCode'),
                $qb->expr()->eq('i.message', ':message'),
                $qb->expr()->gte('i.dateLastSeen', ':minDate')
            ))
            ->setMaxResults(1);

        $qb->setParameter('nodeService', $nodeService);
        $qb->setParameter('zone', $zone);
        $qb->setParameter('severity', $severity);
        $qb->setParameter('account', $account);
        $qb->setParameter('messageCode', $errorCode);
        $qb->setParameter('message', $errorMessage);

        $minDate = clone $date;
        $minDate->modify(self::CORRELATION_INTERVAL);
        $qb->setParameter('minDate', $minDate);

        $incident = $qb->getQuery()->getOneOrNullResult();

        if (! $incident) {
            $incident = new Incident();
            $incident->setNodeService($nodeService);
            $incident->setZone($zone);
            $incident->setDateFirstSeen($date);
            $incident->setSeverity($severity);
            $incident->setAccount($account);
            $incident->setMessageCode($errorCode);
            $incident->setMessage($errorMessage);
        }
        else {
            // Reload from the database, in case it has been read
            $em->refresh($incident);
        }

        $incident->setDateLastSeen($date);
        $incident->setIsRead(false);
        $incident->setIsResolved(false);

        $em->persist($incident);
        $em->flush();

        return $incident;
    }

    /**
     * @param SchedulerResult $schedulerResult
     * @param WorkerResult $workerResult
     * @param Incident $incident
     */
    protected function recordResult (SchedulerResult $schedulerResult, WorkerResult $workerResult, $incident = null) {
        $em = $this->getManager()->getEntityManager();

        $zone = $em->getReference(Zone::class, $this->getConfig('zone'));
//        $nodeService = $em->getReference(NodeService::class, $schedulerResult->getTaskId());
        $nodeService = $this->getNodeServiceFromTaskId($schedulerResult->getTaskId());
        $startTime = new \DateTime('@' . $schedulerResult->getStartTime(), new \DateTimeZone('UTC'));
        $endTime = new \DateTime('@' . $schedulerResult->getEndTime(), new \DateTimeZone('UTC'));
        $severity  = $em->getReference(Severity::class, $workerResult->getStatus());

        //////////////////////////////
        // Create New Result Record //
        //////////////////////////////
        $result = new ResultModel();
        $result->setZone($zone);
        $result->setNodeService($nodeService);
        $result->setStartTime($startTime);
        $result->setEndTime($endTime);
        $result->setElapsedTime($schedulerResult->getElapsedTime());
        $result->setStatusCode($severity);
        $result->setIncident($incident);

        $em->persist($result);


        ////////////////////////////////////////
        // Find Existing Result Latest Record //
        ////////////////////////////////////////
        $qb = $em->createQueryBuilder();
        $qb->select('i')
            ->from(ResultLatest::class, 'i')
            ->innerJoin('i.zone', 'z')
            ->where($qb->expr()->andX(
                $qb->expr()->eq('i.nodeService', ':nodeService'),
                $qb->expr()->eq('i.zone', ':zone')
            ))
            ->setMaxResults(1);

        $qb->setParameter('nodeService', $nodeService);
        $qb->setParameter('zone', $zone);

        $resultLatest = $qb->getQuery()->getOneOrNullResult();

        if (! $resultLatest) {
            $resultLatest = new ResultLatest();
            $resultLatest->setZone($zone);
            $resultLatest->setNodeService($nodeService);
        }

        $resultLatest->setStartTime($startTime);
        $resultLatest->setEndTime($endTime);
        $resultLatest->setElapsedTime($schedulerResult->getElapsedTime());
        $resultLatest->setStatusCode($severity);
        $resultLatest->setIncident($incident);

        $em->persist($resultLatest);

        ///////////////////////////////
        // Add Result Metric Records //
        ///////////////////////////////
        foreach ($workerResult->getMetricCollection()->getMetrics() as $metricName => $metric) {
            $resultMetric = new ResultMetricModel();

            $resultMetric->setResult($result);
            $resultMetric->setFieldName($metricName);

            $metricType = $metric->getType();

            $resultMetric->setMetricType($em->getReference(MetricType::class, $metricType));
            $resultMetric->setValueAuto($metric->getValue());

            $em->persist($resultMetric);
        }

        $em->flush();
        $em->clear(); // Attempting to resolve memory leaks
    }

    protected function getNodeServiceFromTaskId ($taskId) {
        $em = $this->getManager()->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qqb  = $qb
            ->select('ns')
            ->from(NodeService::class, 'ns')
            ->innerJoin(NodeServiceZone::class, 'nsz', Join::WITH, 'nsz.nodeService = ns')
            ->where($qb->expr()->eq('nsz.nodeServiceZoneId', ':nodeServiceZoneId'));

        $qqb->setParameter('nodeServiceZoneId', $taskId);

        $q = $qqb->getQuery();

        $r = $q->getOneOrNullResult();

        if ($r === null) {
            throw new \Exception('Could not find Node Service for task id ' . $taskId);
        }

        return $r;
    }

    /**
     * @param SchedulerTask $task
     * @param SchedulerError|SchedulerResult $result
     * @return Incident|null null if result was success, incident id if it was considered an bad result.
     */
    public function handleWorkerResult (Task $task, SchedulerResult $result) {
        $workerResult = new WorkerResult($result->getResult());

        $incident = null;

        $resultStatus = $workerResult->getStatus();

        // If the result was not a success
        if ($resultStatus != $workerResult::STATUS_SUCCESS) {
            $incident = $this->recordIncident($task, $result, $resultStatus, $workerResult);
        }

        $this->recordResult($result, $workerResult, $incident);

        return $incident;
    }

    /**
     * Run the worker
     */
    public function run () {
        $scheduler = new Scheduler($this->getClient(), $this->getConfig('zone'));

        while ($result = $scheduler->getResultOrError()) {
            try {
                // If the result is an error, there must be a configuration or runtime error
                if ($result instanceof SchedulerError) {
                    /** @var SchedulerError $result */

                    printf("   Error: % 4s: %d - %s\n", $result->getId(), $result->getErrorCode(), $result->getErrorDetails());

                    $task = $scheduler->getTaskEntity($result->getId(), true);

                    $incident = $this->recordIncident($task, $result, 'CONF');

                    $scheduler->pushIncident($incident->getIncidentId());

                    $scheduler->markErrorReported($result);
                }
                // The test was performed and a result is given
                elseif ($result instanceof SchedulerResult) {
                    /** @var SchedulerResult $result */

                    printf("Complete: %s: %s to %s\n", $result->getId(), $result->getStartTime(), $result->getEndTime());

                    $task = $scheduler->getTaskEntity($result->getTaskId(), true);

                    $incident = $this->handleWorkerResult($task, $result);

                    $scheduler->markResultRecorded($result);

                    if ($incident) {
                        $scheduler->pushIncident($incident->getIncidentId());
                    }

                    $scheduler->publishResult($result);
                }
                else {
                    // Unknown type
                    printf("Unknown!\n");
                }

                // Attempting to resolve memory leaks
                $this->getManager()->getEntityManager()->clear();
            }
            catch (\Exception $e) {
                // TODO: record application exception
                echo $e->getMessage();
                throw $e;
            }
        }
    }

    /**
     * Retrieve the config, or a key from it.
     * @param string $key
     * @return array
     */
    public function getConfig ($key = null) {
        if ($key && array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }
        
        return $this->config;
    }
    
    /**
     * @param array $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }
    
    /**
     * @return Client
     */
    public function getClient () {
        return $this->client;
    }
    
    /**
     * @param Client $client
     */
    public function setClient (Client $client) {
        $this->client = $client;
    }
    
    /**
     * @return Manager
     */
    public function getManager () {
        return $this->manager;
    }

    /**
     * @param Manager $manager
     */
    public function setManager (Manager $manager) {
        $this->manager = $manager;
    }
}